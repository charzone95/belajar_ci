<?php

/**
 * Created by PhpStorm.
 * User: ASUS
 * Date: 6/1/2017
 * Time: 11:58
 */
class Base_model extends CI_Model {
    protected $table_name;

    public function __construct() {
        parent::__construct();

        $this->load->database();
    }

    /**
     * Function to get an object based on the "where" conditions
     *
     * @param array $where
     * @return object an object containing the result
     */

    public function getOne($where = []) {
        $query = $this->db->where($where)->get($this->table_name);

        $results = $query->result();

        if ($results) {
            return $results[0];
        } else {
            return null;
        }
    }

    /**
     * Function to get list of data based on the "where" conditions
     *
     * @param array $where
     * @param integer $limit
     * @param integer $offset
     * @return array an array of objects containing the results
     */
    public function getAll($where = [], $limit = null, $offset = null) {
        $query = $this->db->where($where)->get($this->table_name, $limit, $offset);

        return $query->result();
    }


    public function insert($data) {
        return $this->db->insert($this->table_name, $data);
    }

    public function update($data, $where = [], $limit = null) {
        return $this->db->update($this->table_name, $data, $where, $limit);
    }

    public function delete($where, $limit = null) {
        return $this->db->delete($this->table_name, $where, $limit);
    }


}